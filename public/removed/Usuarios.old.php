<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\UserModel;
use App\Models\UserContactsModel;

class Usuarios extends BaseController
{
	public function index()
	{
		$model = new UserModel();
		echo view('admin/templates/header-datatables', ['title' => 'Usuários']);
		echo view('admin/users/index', ['users' => $model->findAll()]);
		echo view('admin/templates/footer-datatables');
	}

	public function create()
	{
		helper(['form']);
		echo view('admin/templates/header', ['title' => 'Cadastro de usuário']);
		echo view('admin/users/add');
		echo view('admin/templates/footer');
	}

	public function add()
	{
		helper(['form']);
		if ($this->validate([
			'name' => ['rules' => 'required|min_length[2]', 'errors' => ['required' => 'O campo é obrigatório!', 'min_length' => 'Nome muito curto!']],
			'surname' => ['rules' => 'required|min_length[2]', 'errors' => ['required' => 'O campo é obrigatório!', 'min_length' => 'Sobrenome muito curto!']],
			'email' => ['rules' => 'required|valid_email|is_unique[users.email]', 'errors' => ['required' => 'O campo é obrigatório!', 'valid_email' => 'O e-mail não é válido!', 'is_unique' => 'E-mail já cadastrado!']],
			'gender' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']]
		])) {
			$model = new UserModel();
			$id = $model->insert([
				'name' => trim($this->request->getVar('name')),
				'surname' => trim($this->request->getVar('surname')),
				'email' => $this->request->getVar('email')
			]);
			$model = new UserContactsModel();
			$model->save(['user_id' => $id]);
		} else {
			echo view('admin/templates/header', ['title' => 'Novo usuário']);
			echo view('admin/users/add', ['validation' => $this->validator]);
			echo view('admin/templates/footer');
		}
	}

	public function photo()
	{
		helper(['form']);
		$model = new UserModel();
		echo view('admin/templates/header', ['title' => 'Perfil']);
		echo view('admin/users/photo', ['user' => $model->getUser(session()->get('id'))]);
		echo view('admin/templates/footer');
		//'photo' => ['photo' => 'uploaded[photo]|max_size[photo,1024]|ext_in[photo,jpg,jpeg,png]'],
		//var_dump($this->request->getPost('photo'));		
		//'photo' => ['rules' => 'max_size[photo,1024]|ext_in[photo,jpg,jpeg]', 'errors' => ['ext_in' => 'Arquivo inválido!']],
	}

	public function profile()
	{
		helper(['form']);
		$model = new UserModel();
		if ($this->request->getMethod() === 'post' && $this->validate([
			'name' => ['rules' => 'required|min_length[2]', 'errors' => ['required' => 'O campo é obrigatório!', 'min_length' => 'Nome muito curto!']],
			'surname' => ['rules' => 'required|min_length[2]', 'errors' => ['required' => 'O campo é obrigatório!', 'min_length' => 'Sobrenome muito curto!']],
			'email' => ['rules' => 'required|valid_email', 'errors' => ['required' => 'O campo é obrigatório!', 'valid_email' => 'O e-mail não é válido!']],
			'gender' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'zip_code' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'street' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'city' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'state' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'number' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'district' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
		])) {
			if ($this->request->getFile('photo')->isValid()) {
				print_r($this->request->getFile('photo'));
			} else {
				return redirect()->to(base_url('admin/usuarios'));
			}
			$model = new UserContactsModel();
			$model->save([
				'user_id' => session()->get('id'),
				'zip_code' => $this->request->getPost('zip_code'),
				'street' => trim($this->request->getPost('street')),
				'number' => $this->request->getPost('number'),
				'complement' => trim($this->request->getPost('complement')) === '' ? '—' : $this->request->getPost('complement'),
				'district' => trim($this->request->getPost('district')),
				'city' => trim($this->request->getPost('city')),
				'state' => $this->request->getPost('state'),
				'cell' => trim($this->request->getPost('cell')) === '' ? '—' : $this->request->getPost('cell'),
				'whatsapp' => trim($this->request->getPost('whatsapp')) === '' ? '—' : $this->request->getPost('whatsapp'),
				'phone' => trim($this->request->getPost('phone')) === '' ? '—' : $this->request->getPost('phone'),
				'facebook' => trim($this->request->getPost('facebook')) === '' ? '—' : $this->request->getPost('facebook'),
				'twitter' => trim($this->request->getPost('twitter')) === '' ? '—' : $this->request->getPost('twitter'),
				'instagram' => trim($this->request->getPost('instagram')) === '' ? '—' : $this->request->getPost('instagram')
			]);
			session()->set([
				'id' => session()->get('id'),
				'name' => $this->request->getPost('name') . ' ' . $this->request->getPost('surname'),
				'email' => $this->request->getPost('email'),
				'logged_in' => true
			]);
			return redirect()->to(base_url('admin/usuarios/perfil'));
		} else {
			echo view('admin/templates/header', ['title' => 'Perfil']);
			echo view('admin/users/profile', ['validation' => $this->validator, 'user' => $model->getUser(session()->get('id'))]);
			echo view('admin/templates/footer');
		}
	}

	public function perfil()
	{
		helper(['form']);
		$session = session();
		$user = new UserModel();
		if ($this->request->getMethod() == "post" && $this->validate([
			'name' => ['rules' => 'required|min_length[2]', 'errors' => ['required' => 'O campo é obrigatório!', 'min_length' => 'Nome muito curto!']],
			'surname' => ['rules' => 'required|min_length[2]', 'errors' => ['required' => 'O campo é obrigatório!', 'min_length' => 'Sobrenome muito curto!']],
			'email' => ['rules' => 'required|valid_email', 'errors' => ['required' => 'O campo é obrigatório!', 'valid_email' => 'O e-mail não é válido!']],
			'gender' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'zip_code' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'street' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'city' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'state' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'number' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'district' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
		])) {
			$file = $this->request->getFile("file");
			if ($file->isValid()) {
				if ($this->validate([
					"file" => ["rules" => "uploaded[file]|max_size[file,1024]|is_image[file]|mime_in[file,image/jpg,image/jpeg,image/gif,image/png]",]
				])) {
					$newName = $file->getRandomName();
					$file->move('uploads/users/' . $session->get('id'), $newName);
					$user->save([
						'id' => $session->get('id'),
						'name' => trim($this->request->getPost('name')),
						'surname' => trim($this->request->getPost('surname')),
						'email' => $this->request->getPost('email'),
						'gender' => $this->request->getPost('gender'),
						'photo' => 'uploads/users/' . $session->get('id') . '/' . $newName
					]);
					$session->set(['photo' => 'uploads/users/' . $session->get('id') . '/' . $newName]);
					$session->setFlashdata("success", "Perfil atualizado com sucesso!");
				}
			} else {
				$user->save([
					'id' => $session->get('id'),
					'name' => trim($this->request->getPost('name')),
					'surname' => trim($this->request->getPost('surname')),
					'email' => $this->request->getPost('email'),
					'gender' => $this->request->getPost('gender')
				]);
				$session->setFlashdata("success", "Perfil atualizado com sucesso!");
			}
			$contact = new UserContactsModel();
			$contact->save([
				'user_id' => $session->get('id'),
				'zip_code' => $this->request->getPost('zip_code'),
				'street' => trim($this->request->getPost('street')),
				'number' => $this->request->getPost('number'),
				'complement' => trim($this->request->getPost('complement')) === '' ? '—' : $this->request->getPost('complement'),
				'district' => trim($this->request->getPost('district')),
				'city' => trim($this->request->getPost('city')),
				'state' => $this->request->getPost('state'),
				'cell' => trim($this->request->getPost('cell')) === '' ? '—' : $this->request->getPost('cell'),
				'whatsapp' => trim($this->request->getPost('whatsapp')) === '' ? '—' : $this->request->getPost('whatsapp'),
				'phone' => trim($this->request->getPost('phone')) === '' ? '—' : $this->request->getPost('phone'),
				'facebook' => trim($this->request->getPost('facebook')) === '' ? '—' : $this->request->getPost('facebook'),
				'twitter' => trim($this->request->getPost('twitter')) === '' ? '—' : $this->request->getPost('twitter'),
				'instagram' => trim($this->request->getPost('instagram')) === '' ? '—' : $this->request->getPost('instagram')
			]);
			$session->set(['name' => trim($this->request->getPost('name')) . ' ' . trim($this->request->getPost('surname'))]);
			echo view('admin/templates/header', ['title' => 'Perfil']);
			echo view('admin/users/profile', ['validation' => $this->validator, 'user' => $user->getUser($session->get('id'))]);
			echo view('admin/templates/footer');
		} else {
			echo view('admin/templates/header', ['title' => 'Perfil']);
			echo view('admin/users/profile', ['validation' => $this->validator, 'user' => $user->getUser($session->get('id'))]);
			echo view('admin/templates/footer');
		}
	}

	public function perfilold()
	{
		helper(['form']);
		$session = session();
		$user = new UserModel();
		if ($this->request->getMethod() == "post" && $this->validate([
			'name' => ['rules' => 'required|min_length[2]', 'errors' => ['required' => 'O campo é obrigatório!', 'min_length' => 'Nome muito curto!']],
			'surname' => ['rules' => 'required|min_length[2]', 'errors' => ['required' => 'O campo é obrigatório!', 'min_length' => 'Sobrenome muito curto!']],
			'email' => ['rules' => 'required|valid_email', 'errors' => ['required' => 'O campo é obrigatório!', 'valid_email' => 'O e-mail não é válido!']],
			'gender' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'zip_code' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'street' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'city' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'state' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'number' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'district' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
		])) {
			$file = $this->request->getFile("file");
			if ($file->isValid()) {
				if ($this->validate([
					"file" => ["rules" => "uploaded[file]|max_size[file,1024]|is_image[file]|mime_in[file,image/jpg,image/jpeg,image/gif,image/png]",]
				])) {
					$newName = $file->getRandomName();
					$file->move('uploads/users/' . $session->get('id'), $newName);
					$user->save([
						'id' => $session->get('id'),
						'name' => trim($this->request->getPost('name')),
						'surname' => trim($this->request->getPost('surname')),
						'email' => $this->request->getPost('email'),
						'gender' => $this->request->getPost('gender'),
						'photo' => 'uploads/users/' . $session->get('id') . '/' . $newName
					]);
					$session->set(['photo' => 'uploads/users/' . $session->get('id') . '/' . $newName]);
					$session->setFlashdata("success", "Perfil atualizado com sucesso!");
				}
			} else {
				$user->save([
					'id' => $session->get('id'),
					'name' => trim($this->request->getPost('name')),
					'surname' => trim($this->request->getPost('surname')),
					'email' => $this->request->getPost('email'),
					'gender' => $this->request->getPost('gender')
				]);
				$session->setFlashdata("success", "Perfil atualizado com sucesso!");
			}
			$contact = new UserContactsModel();
			$contact->save([
				'user_id' => $session->get('id'),
				'zip_code' => $this->request->getPost('zip_code'),
				'street' => trim($this->request->getPost('street')),
				'number' => $this->request->getPost('number'),
				'complement' => trim($this->request->getPost('complement')) === '' ? '—' : $this->request->getPost('complement'),
				'district' => trim($this->request->getPost('district')),
				'city' => trim($this->request->getPost('city')),
				'state' => $this->request->getPost('state'),
				'cell' => trim($this->request->getPost('cell')) === '' ? '—' : $this->request->getPost('cell'),
				'whatsapp' => trim($this->request->getPost('whatsapp')) === '' ? '—' : $this->request->getPost('whatsapp'),
				'phone' => trim($this->request->getPost('phone')) === '' ? '—' : $this->request->getPost('phone'),
				'facebook' => trim($this->request->getPost('facebook')) === '' ? '—' : $this->request->getPost('facebook'),
				'twitter' => trim($this->request->getPost('twitter')) === '' ? '—' : $this->request->getPost('twitter'),
				'instagram' => trim($this->request->getPost('instagram')) === '' ? '—' : $this->request->getPost('instagram')
			]);
			$session->set(['name' => trim($this->request->getPost('name')) . ' ' . trim($this->request->getPost('surname'))]);
			echo view('admin/templates/header', ['title' => 'Perfil']);
			echo view('admin/users/profile', ['validation' => $this->validator, 'user' => $user->getUser($session->get('id'))]);
			echo view('admin/templates/footer');
		} else {
			echo view('admin/templates/header', ['title' => 'Perfil']);
			echo view('admin/users/profile', ['validation' => $this->validator, 'user' => $user->getUser($session->get('id'))]);
			echo view('admin/templates/footer');
		}
	}

	//$userBD = $user->find($session->get('id'));
	//echo '<pre>';
	//echo print_r($userBD->password);
	/*if (isset($userBD) && password_verify($this->request->getPost('atual'), $userBD->password)) {
				if ($user->save([
					'id' => $session->get('id'),
					'email' => $this->request->getPost('email'),
					'password' => password_hash($this->request->getPost('password'), PASSWORD_DEFAULT, ['cost' => 8])
				])) {
					$session->set(['email' => trim($this->request->getPost('email'))]);
					$session->setFlashdata("success", "Dados da conta atualizados com sucesso!");
				} else {
					$session->setFlashdata("error", "Dados da conta não foram atualizados!");
				}
			} else {
				$session->setFlashdata("error", "Dados da conta não foram atualizados!");
			}*/
}
