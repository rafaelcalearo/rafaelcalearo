 <div class="container-fluid">
     <nav aria-label="breadcrumb">
         <ol class="breadcrumb small">
             <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">casa</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/dashboard') ?>">dashboard</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/usuarios') ?>">usuários</a></li>
             <li class="breadcrumb-item active" aria-current="page">perfil</li>
         </ol>
     </nav>
     <div class="d-sm-flex align-items-center justify-content-between mb-4">
         <h1 class="h4 mb-0 text-gray-800"><strong>PERFIL</strong></h1>
         <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">ALTERAR SENHA</a>
     </div>
     <div class="row">
         <div class="col-xl-5 col-lg-6">
             <div class="card shadow mb-4">
                 <div class="card-header mt-4 text-center">
                     <img src="<?= base_url('img/profile/' . $user[0]->photo) ?>" class="rounded-circle mb-2" alt="imagem do perfil" width="150" height="150">
                     <h5 class="text-gray-900 mb-0"><?= $user[0]->name ?> <?= $user[0]->surname ?></h5>
                     <p class="text-gray-500 mt-0">
                         <?= $user[0]->profile === "ADMIN" ? 'Administrador' : 'Usuário' ?> do sistema
                     </p>
                 </div>
                 <div class="card-body">
                     <small><span class="small">ENDEREÇO DE E-MAIL</span></small>
                     <h6 class="text-gray-900"><strong><?= $user[0]->email ?></strong></h6>
                     <small><span class="small">CELULAR</span></small>
                     <h6 class="text-gray-900"><strong><?= $user[0]->cell ?></strong></h6>
                     <small><span class="small">WHATSAPP</span></small>
                     <h6 class="text-gray-900"><strong><?= $user[0]->whatsapp ?></strong></h6>
                     <small><span class="small">TELEFONE RESIDENCIAL</span></small>
                     <h6 class="text-gray-900"><strong><?= $user[0]->phone ?></strong></h6>
                     <small><span class="small">ENDEREÇO</span></small>
                     <h6 class="text-gray-900">
                         <strong>
                             <?= $user[0]->street === '—' ? '—' : $user[0]->street . ', n. ' . $user[0]->number
                                    . ($user[0]->complement === '—' ? '' : ' (' . $user[0]->complement . ')') ?>
                         </strong>
                     </h6>
                     <small><span class="small">BAIRRO</span></small>
                     <h6 class="text-gray-900"><strong><?= $user[0]->district ?></strong></h6>
                     <small><span class="small">CEP</span></small>
                     <h6 class="text-gray-900"><strong><?= $user[0]->zip_code ?></strong></h6>
                     <small><span class="small">CIDADE (UF)</span></small>
                     <h6 class="text-gray-900">
                         <strong><?= $user[0]->city === '—' ? '—' : $user[0]->city . ' (' . $user[0]->state . ')' ?></strong>
                     </h6>
                     <div class="map-box">
                         <small><span class="small">MAPA DO ENDEREÇO</span></small>
                         <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d65193779.752580315!2d-2.3492343561722606!3d4.198242496589812!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1624639885461!5m2!1spt-BR!2sbr" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
                     </div>
                     <small><span class="small">REDES SOCIAIS</span></small>
                     <br />
                     <?php
                        if ($user[0]->facebook && $user[0]->instagram && $user[0]->twitter === '—') {
                            echo '<h6 class="text-gray-900"><strong>—</strong></h6>';
                        }
                        if ($user[0]->facebook != '—') {
                            echo '<a href="<?= $user[0]->facebook ?>" target="_blank" class="btn btn-circle btn-secondary mr-1"><i class="fab fa-facebook-f"></i></a>';
                        }
                        if ($user[0]->instagram != '—') {
                            echo '<a href="<?= $user[0]->instagram ?>" target="_blank" class="btn btn-circle btn-secondary mr-1"><i class="fab fa-instagram"></i></a>';
                        }
                        if ($user[0]->twitter != '—') {
                            echo '<a href="<?= $user[0]->twitter ?>" target="_blank" class="btn btn-circle btn-secondary"><i class="fab fa-twitter"></i></a>';
                        }
                        ?>
                 </div>
             </div>
         </div>
         <div class="col-xl-7 col-lg-6">
             <div class="card shadow mb-4">
                 <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary">ATUALIZAR PERFIL</h6>
                     <?php $validation = \Config\Services::validation(); ?>
                 </div>
                 <div class="card-body">
                     <form method="post" action="<?= base_url('admin/usuarios/perfil') ?>" enctype="multipart/form-data">
                         <?= csrf_field() ?>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label for="name"><strong>Nome</strong> <span class="text-danger">*</span></label>
                                 <input type="text" name="name" class="form-control form-control-sm<?= $validation->getError('name') ? ' is-invalid' : '' ?>" value="<?= set_value('name', $user[0]->name) ?>" id="name" />
                                 <span class='small text-danger'><?= $validation->getError('name') ?></span>
                             </div>
                             <div class="form-group col-md-6">
                                 <label for="surname"><strong>Sobrenome</strong> <span class="text-danger">*</span></label>
                                 <input type="text" name="surname" class="form-control form-control-sm<?= $validation->getError('surname') ? ' is-invalid' : '' ?>" value="<?= set_value('surname', $user[0]->surname) ?>" id="surname" />
                                 <span class='small text-danger'><?= $validation->getError('surname') ?></span>
                             </div>
                         </div>
                         <div class="form-group">
                             <label for="email"><strong>E-mail</strong> <span class="text-danger">*</span></label>
                             <input type="email" name="email" class="form-control form-control-sm<?= $validation->getError('email') ? ' is-invalid' : '' ?>" value="<?= set_value('email', $user[0]->email) ?>" id="email" />
                             <span class='small text-danger'><?= $validation->getError('email') ?></span>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label for="gender"><strong>Gênero</strong></label>
                                 <span class="form-control form-control-sm pl-5<?= $validation->getError('gender') ? ' is-invalid' : '' ?>">
                                     <input type="radio" name="gender" class="form-check-input" value="f" id="gender" <?= ($user[0]->gender == 'f' || set_value('gender') === 'f') ? 'checked' : '' ?> />
                                     <label for="gender" class="form-check-label">Feminino</label>
                                 </span>
                             </div>
                             <div class="form-group col-md-6">
                                 <label for="gender">&nbsp;</label>
                                 <span class="form-control form-control-sm pl-5<?= $validation->getError('gender') ? ' is-invalid' : '' ?>">
                                     <input type="radio" name="gender" class="form-check-input" value="m" id="gender" <?= ($user[0]->gender == 'm' || set_value('gender') === 'm') ? 'checked' : '' ?> />
                                     <label for="gender" class="form-check-label">Masculino</label>
                                 </span>
                             </div>
                         </div>
                         <hr>
                         <div class="form-row">
                             <div class="form-group col-md-4">
                                 <label for="zip_code"><strong>CEP</strong> <span class="text-danger">*</span></label>
                                 <input type="text" name="zip_code" class="form-control form-control-sm<?= $validation->getError('zip_code') ? ' is-invalid' : '' ?>" value="<?= set_value('zip_code', $user[0]->zip_code === '—' ? '' : $user[0]->zip_code) ?>" id="zip_code" />
                                 <span class='small text-danger'><?= $validation->getError('zip_code') ?></span>
                             </div>
                             <div class="form-group col-md-8">
                                 <label for="street"><strong>Rua</strong> <span class="text-danger">*</span></label>
                                 <input type="text" name="street" class="form-control form-control-sm<?= $validation->getError('street') ? ' is-invalid' : '' ?>" value="<?= set_value('street', $user[0]->street === '—' ? '' : $user[0]->street) ?>" id="street" />
                                 <span class='small text-danger'><?= $validation->getError('street') ?></span>
                             </div>
                             <div class="form-group col-md-8">
                                 <label for="cidade"><strong>Cidade</strong> <span class="text-danger">*</span></label>
                                 <input type="text" name="city" class="form-control form-control-sm<?= $validation->getError('city') ? ' is-invalid' : '' ?>" value="<?= set_value('city', $user[0]->city === '—' ? '' : $user[0]->city) ?>" id="city" />
                                 <span class='small text-danger'><?= $validation->getError('city') ?></span>
                             </div>
                             <div class="form-group col-md-4">
                                 <label for="state"><strong>UF</strong> <span class="text-danger">*</span></label>
                                 <input type="text" name="state" class="form-control form-control-sm<?= $validation->getError('state') ? ' is-invalid' : '' ?>" value="<?= set_value('state', $user[0]->state === '—' ? '' : $user[0]->state) ?>" id="state" />
                                 <span class='small text-danger'><?= $validation->getError('state') ?></span>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-4">
                                 <label for="number"><strong>Número</strong> <span class="text-danger">*</span></label>
                                 <input type="number" name="number" class="form-control form-control-sm<?= $validation->getError('number') ? ' is-invalid' : '' ?>" value="<?= set_value('number', $user[0]->number === '—' ? '' : $user[0]->number) ?>" id="number" />
                                 <span class='small text-danger'><?= $validation->getError('number') ?></span>
                             </div>
                             <div class="form-group col-md-4">
                                 <label for="complement"><strong>Complemento</strong></label>
                                 <input type="text" name="complement" class="form-control form-control-sm" value="<?= set_value('complement', $user[0]->complement === '—' ? null : $user[0]->complement) ?>" id="complement" />
                             </div>
                             <div class="form-group col-md-4">
                                 <label for="district"><strong>Bairro</strong> <span class="text-danger">*</span></label>
                                 <input type="text" name="district" class="form-control form-control-sm<?= $validation->getError('district') ? ' is-invalid' : '' ?>" value="<?= set_value('district', $user[0]->district === '—' ? '' : $user[0]->district) ?>" id="district" />
                                 <span class='small text-danger'><?= $validation->getError('district') ?></span>
                             </div>
                         </div>
                         <hr>
                         <div class="form-row">
                             <div class="form-group col-md-4">
                                 <label for="cell"><strong>Celular</strong></label>
                                 <input type="text" name="cell" class="form-control form-control-sm" value="<?= set_value('cell', $user[0]->cell === '—' ? null : $user[0]->cell) ?>" id="cell" />
                             </div>
                             <div class="form-group col-md-4">
                                 <label for="whatsapp"><strong>WhatsApp</strong></label>
                                 <input type="text" name="whatsapp" class="form-control form-control-sm" value="<?= set_value('whatsapp', $user[0]->whatsapp === '—' ? null : $user[0]->whatsapp) ?>" id="whatsapp" />
                             </div>
                             <div class="form-group col-md-4">
                                 <label for="phone"><strong>Telefone residencial</strong></label>
                                 <input type="text" name="phone" class="form-control form-control-sm" value="<?= set_value('phone', $user[0]->phone === '—' ? null : $user[0]->phone) ?>" id="phone" />
                             </div>
                         </div>
                         <hr>
                         <div class="form-group">
                             <label for="photo"><strong>Foto (Perfil)</strong>
                                 <span class="small">· ideal 150 x 150 pixels</span>
                             </label>
                             <input type="file" name="photo" class="form-control-file form-control-sm" id="photo" />
                             <span class='small text-danger mt-1'><?= $validation->getError('photo') ?></span>
                         </div>
                         <hr>
                         <div class="form-row">
                             <div class="form-group col-md-4">
                                 <label for="facebook"><strong>Facebook (URL)</strong></label>
                                 <input type="text" name="facebook" class="form-control form-control-sm" value="<?= set_value('facebook', $user[0]->facebook === '—' ? null : $user[0]->facebook) ?>" id="facebook" />
                             </div>

                             <div class="form-group col-md-4">
                                 <label for="twitter"><strong>Twitter (URL)</strong></label>
                                 <input type="text" name="twitter" class="form-control form-control-sm" value="<?= set_value('twitter', $user[0]->twitter === '—' ? null : $user[0]->twitter) ?>" id="twitter" />
                             </div>
                             <div class="form-group col-md-4">
                                 <label for="instagram"><strong>Instagram (URL)</strong></label>
                                 <input type="text" name="instagram" class="form-control form-control-sm" value="<?= set_value('instagram', $user[0]->instagram === '—' ? null : $user[0]->instagram) ?>" id="instagram" />
                             </div>
                         </div>
                         <hr>
                         <div class="form-row">
                             <div class="form-group col-md-6"><button class="btn btn-dark btn-sm col-12" type="submit">
                                     <i class="mdi mdi-cached"></i> ATUALIZAR</button></div>
                             <div class="form-group col-md-6"> <a href="/admin/usuarios" class="btn btn-secondary btn-sm col-12" type="reset">
                                     <i class="mdi mdi-close"></i> CANCELAR</a></div>
                         </div>
                     </form>
                 </div>
                 <div class="card-footer text-danger small"><em>* campos obrigatórios!</em></div>
             </div>
         </div>
     </div>
 </div>