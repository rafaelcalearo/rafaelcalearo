 <div class="container-fluid">
     <nav aria-label="breadcrumb">
         <ol class="breadcrumb small">
             <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">casa</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/dashboard') ?>">dashboard</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/usuarios') ?>">usuários</a></li>
             <li class="breadcrumb-item active" aria-current="page">perfil</li>
         </ol>
     </nav>
     <div class="d-sm-flex align-items-center justify-content-between mb-4">
         <h1 class="h4 mb-0 text-gray-800"><strong>PERFIL</strong></h1>
         <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">ALTERAR SENHA</a>
     </div>
     <div class="row">
         <div class="col-xl-5 col-lg-6">
             <div class="card shadow mb-4">
                 <div class="card-header mt-4 text-center">
                     <img src="<?= base_url('img/profile/' . $user[0]->photo) ?>" class="rounded-circle mb-2" alt="imagem do perfil" width="150" height="150">
                     <h5 class="text-gray-900 mb-0"><?= $user[0]->name ?> <?= $user[0]->surname ?></h5>
                     <p class="text-gray-500 mt-0">
                         <?= $user[0]->profile === "ADMIN" ? 'Administrador' : 'Usuário' ?> do sistema
                     </p>
                 </div>
                 <div class="card-body">
                     <small><span class="small">ENDEREÇO DE E-MAIL</span></small>
                     <h6 class="text-gray-900"><strong><?= $user[0]->email ?></strong></h6>
                     <small><span class="small">CELULAR</span></small>
                     <h6 class="text-gray-900"><strong><?= $user[0]->cell ?></strong></h6>
                     <small><span class="small">WHATSAPP</span></small>
                     <h6 class="text-gray-900"><strong><?= $user[0]->whatsapp ?></strong></h6>
                     <small><span class="small">TELEFONE RESIDENCIAL</span></small>
                     <h6 class="text-gray-900"><strong><?= $user[0]->phone ?></strong></h6>
                     <small><span class="small">ENDEREÇO</span></small>
                     <h6 class="text-gray-900">
                         <strong>
                             <?= $user[0]->street === '—' ? '—' : $user[0]->street . ', n. ' . $user[0]->number
                                    . ($user[0]->complement === '—' ? '' : ' (' . $user[0]->complement . ')') ?>
                         </strong>
                     </h6>
                     <small><span class="small">BAIRRO</span></small>
                     <h6 class="text-gray-900"><strong><?= $user[0]->district ?></strong></h6>
                     <small><span class="small">CEP</span></small>
                     <h6 class="text-gray-900"><strong><?= $user[0]->zip_code ?></strong></h6>
                     <small><span class="small">CIDADE (UF)</span></small>
                     <h6 class="text-gray-900">
                         <strong><?= $user[0]->city === '—' ? '—' : $user[0]->city . ' (' . $user[0]->state . ')' ?></strong>
                     </h6>
                     <div class="map-box">
                         <small><span class="small">MAPA DO ENDEREÇO</span></small>
                         <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d65193779.752580315!2d-2.3492343561722606!3d4.198242496589812!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1624639885461!5m2!1spt-BR!2sbr" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
                     </div>
                     <small><span class="small">REDES SOCIAIS</span></small>
                     <br />
                     <?php
                        if ($user[0]->facebook && $user[0]->instagram && $user[0]->twitter === '—') {
                            echo '<h6 class="text-gray-900"><strong>—</strong></h6>';
                        }
                        if ($user[0]->facebook != '—') {
                            echo '<a href="<?= $user[0]->facebook ?>" target="_blank" class="btn btn-circle btn-secondary mr-1"><i class="fab fa-facebook-f"></i></a>';
                        }
                        if ($user[0]->instagram != '—') {
                            echo '<a href="<?= $user[0]->instagram ?>" target="_blank" class="btn btn-circle btn-secondary mr-1"><i class="fab fa-instagram"></i></a>';
                        }
                        if ($user[0]->twitter != '—') {
                            echo '<a href="<?= $user[0]->twitter ?>" target="_blank" class="btn btn-circle btn-secondary"><i class="fab fa-twitter"></i></a>';
                        }
                        ?>
                 </div>
             </div>
         </div>
         <div class="col-xl-7 col-lg-6">
             <div class="card shadow mb-4">
                 <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary">ATUALIZAR PERFIL</h6>
                     <?php $validation = \Config\Services::validation(); ?>
                 </div>
                 <div class="card-body">
                     <form method="post" action="<?= base_url('admin/perfil/editar-foto/atualizar') ?>" enctype="multipart/form-data">
                         <?= csrf_field() ?>                        
                         <div class="form-group">
                             <label for="file"><strong>Foto (Perfil)</strong>
                                 <span class="small">· ideal 150 x 150 pixels</span>
                             </label>
                             <input type="file" name="file" class="form-control-file form-control-sm" id="file" />
                             <span class='small text-danger mt-1'><?= $validation->getError('file') ?></span>
                         </div>                        
                         <div class="form-row">
                             <div class="form-group col-md-6"><button class="btn btn-dark btn-sm col-12" type="submit">
                                     <i class="mdi mdi-cached"></i> ATUALIZAR</button></div>
                             <div class="form-group col-md-6"> <a href="/admin/usuarios" class="btn btn-secondary btn-sm col-12" type="reset">
                                     <i class="mdi mdi-close"></i> CANCELAR</a></div>
                         </div>
                     </form>
                 </div>
                 <div class="card-footer text-danger small"><em>* campos obrigatórios!</em></div>
             </div>
         </div>
     </div>
 </div>