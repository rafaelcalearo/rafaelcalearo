-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 16-Jul-2021 às 23:37
-- Versão do servidor: 10.4.17-MariaDB
-- versão do PHP: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `rafael_calearo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `usercontacts`
--

CREATE TABLE `usercontacts` (
  `user_id` int(5) NOT NULL,
  `zip_code` varchar(9) DEFAULT NULL,
  `street` varchar(191) DEFAULT NULL,
  `number` varchar(10) DEFAULT NULL,
  `complement` varchar(191) DEFAULT NULL,
  `district` varchar(191) DEFAULT NULL,
  `city` varchar(191) DEFAULT NULL,
  `state` varchar(191) DEFAULT NULL,
  `cell` varchar(15) DEFAULT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `phone` varchar(14) DEFAULT NULL,
  `instagram` varchar(191) DEFAULT NULL,
  `facebook` varchar(191) DEFAULT NULL,
  `twitter` varchar(191) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usercontacts`
--

INSERT INTO `usercontacts` (`user_id`, `zip_code`, `street`, `number`, `complement`, `district`, `city`, `state`, `cell`, `whatsapp`, `phone`, `instagram`, `facebook`, `twitter`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '96025-240', 'Rua Marquês de Barbacena', '110', '', 'Fragata', 'Pelotas', 'RS', '(53) 98154-2211', '', '', '', 'alessandravizeusoares', '', '2021-06-21 13:30:05', '2021-07-13 17:04:41', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(5) NOT NULL,
  `name` varchar(191) NOT NULL,
  `surname` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `password` varchar(191) NOT NULL,
  `profile` varchar(10) NOT NULL,
  `photo` varchar(191) DEFAULT NULL,
  `gender` varchar(1) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `email`, `password`, `profile`, `photo`, `gender`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Alessandra', 'Vizeu', 'alessandra.vizeu@hotmail.com', '$2y$08$XpNBvPsqzeliUA7V40OOQOAHFl0BcwM4gXnMNVggnDqjKB2i/qtcy', 'USER', 'uploads/users/1/1625527414_fa72c32d440d5ae0e64d.jpg', 'f', '2021-06-21 18:24:17', '2021-07-13 16:53:04', NULL);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `usercontacts`
--
ALTER TABLE `usercontacts`
  ADD PRIMARY KEY (`user_id`);

--
-- Índices para tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
