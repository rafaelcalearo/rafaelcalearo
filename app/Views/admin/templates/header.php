<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>RAFAEL CALEARO | <?= $title ?></title>
    <link href="<?php echo base_url('/vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="<?php echo base_url('css/sb-admin-2.css') ?>" rel="stylesheet">
    <?= $datatables ? '<link href="' . base_url('/vendor/datatables/dataTables.bootstrap4.css') . '" rel="stylesheet">' : '' ?>
</head>

<body id="page-top">
    <?php if (session()->get("success")) { ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <?= session()->get("success") ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php }
    if (session()->get("error")) { ?>
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <?= session()->get("error") ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php } ?>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php echo view('admin/components/sidebar', ['uri' => service('uri')]) ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php echo view('admin/components/topbar') ?>