<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item<?= $uri->getSegment(2) === 'dashboard' ? ' active' : '' ?>">
        <a class="nav-link" href="/admin/dashboard">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>DASHBOARD</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Charts fa-chart-area-->
    <li class="nav-item<?= $uri->getSegment(2) === 'usuarios' ? ' active' : '' ?>">
        <a class="nav-link" href="/admin/usuarios">
            <i class="fas fa-user-friends"></i>
            <span>USUÁRIOS</span></a>
    </li>

    <!-- Nav Item - Tables -->
    <li class="nav-item<?= $uri->getSegment(2) === 'categorias' ? ' active' : '' ?>">
        <a class="nav-link" href="/admin/categorias">
            <i class="fas fa-tags"></i>
            <span>CATEGORIAS</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
<!-- End of Sidebar -->