 <div class="container-fluid">
     <nav aria-label="breadcrumb">
         <ol class="breadcrumb small">
             <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">casa</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/dashboard') ?>">dashboard</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/perfil') ?>">perfil</a></li>
             <li class="breadcrumb-item active" aria-current="page">editar perfil</li>
         </ol>
     </nav>
     <div class="d-sm-flex align-items-center justify-content-between mb-4">
         <h1 class="h4 mb-0 text-gray-800"><strong>DADOS BÁSICOS</strong></h1>
     </div>
     <div class="row">
         <div class="col-xl-4 col-lg-6">
             <div class="card shadow h-100">
                 <div class="card-header mt-4 text-center">
                     <img src="<?= base_url($user[0]->photo) ?>" class="rounded-circle mb-2" alt="imagem do perfil" width="150" height="150">
                     <h5 class="text-gray-900 mb-0"><?= $user[0]->name ?> <?= $user[0]->surname ?></h5>
                     <p class="text-gray-500 mt-0">
                         <em><?= $user[0]->profile === "ADMIN" ? 'Administrador' : 'Usuário' ?> do sistema</em>
                     </p>
                 </div>
                 <div class="card-body">
                     <ul class="list-group list-group-flush">
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil') ?>">Perfil</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-perfil') ?>"><strong>Dados básicos</strong></a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-foto') ?>">Foto</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-endereco') ?>">Endereço</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-conta') ?>">Conta</a>
                         </li>
                         <li class="list-group-item"><a href="">Encerrar conta</a></li>
                     </ul>
                 </div>
             </div>
         </div>
         <div class="col-xl-8 col-lg-6">
             <div class="card shadow h-100">
                 <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary">ALTERAR DADOS BÁSICOS</h6>
                     <?php $validation = \Config\Services::validation(); ?>
                 </div>
                 <div class="card-body">
                     <form method="post" action="<?= base_url('admin/perfil/editar-perfil') ?>" enctype="multipart/form-data">
                         <?= csrf_field() ?>
                         <div class="form-group">
                             <label for="name"><strong>Nome</strong> <span class="text-danger">*</span></label>
                             <input type="text" name="name" class="form-control<?= $validation->getError('name') ? ' is-invalid' : '' ?>" value="<?= set_value('name', $user[0]->name) ?>" id="name" />
                             <span class='small text-danger'><?= $validation->getError('name') ?></span>
                         </div>
                         <div class="form-group">
                             <label for="surname"><strong>Sobrenome</strong> <span class="text-danger">*</span></label>
                             <input type="text" name="surname" class="form-control<?= $validation->getError('surname') ? ' is-invalid' : '' ?>" value="<?= set_value('surname', $user[0]->surname) ?>" id="surname" />
                             <span class='small text-danger'><?= $validation->getError('surname') ?></span>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label for="gender"><strong>Gênero</strong> <span class="text-danger">*</span></label>
                                 <span class="form-control pl-5<?= $validation->getError('gender') ? ' is-invalid' : '' ?>">
                                     <input type="radio" name="gender" class="form-check-input" value="f" id="gender" <?= ($user[0]->gender == 'f' || set_value('gender') === 'f') ? 'checked' : '' ?> />
                                     <label for="gender" class="form-check-label">Feminino</label>
                                 </span>
                             </div>
                             <div class="form-group col-md-6">
                                 <label for="gender">&nbsp;</label>
                                 <span class="form-control pl-5<?= $validation->getError('gender') ? ' is-invalid' : '' ?>">
                                     <input type="radio" name="gender" class="form-check-input" value="m" id="gender" <?= ($user[0]->gender == 'm' || set_value('gender') === 'm') ? 'checked' : '' ?> />
                                     <label for="gender" class="form-check-label">Masculino</label>
                                 </span>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-4">
                                 <label for="cell"><strong>Celular</strong></label>
                                 <input type="text" name="cell" class="form-control<?= $validation->getError('cell') ? ' is-invalid' : '' ?>" value="<?= set_value('cell', $user[0]->cell) ?>" id="cell" />
                                 <span class='small text-danger'><?= $validation->getError('cell') ?></span>
                             </div>
                             <div class="form-group col-md-4">
                                 <label for="whatsapp"><strong>WhatsApp</strong></label>
                                 <input type="text" name="whatsapp" class="form-control<?= $validation->getError('whatsapp') ? ' is-invalid' : '' ?>" value="<?= set_value('whatsapp', $user[0]->whatsapp) ?>" id="whatsapp" />
                                 <span class='small text-danger'><?= $validation->getError('whatsapp') ?></span>
                             </div>
                             <div class="form-group col-md-4">
                                 <label for="phone"><strong>Telefone fixo</strong></label>
                                 <input type="text" name="phone" class="form-control<?= $validation->getError('phone') ? ' is-invalid' : '' ?>" value="<?= set_value('phone', $user[0]->phone) ?>" id="phone" />
                                 <span class='small text-danger'><?= $validation->getError('phone') ?></span>
                             </div>
                         </div>
                         <hr>
                         <label for="sociais" class="form-label"><strong>Redes sociais</strong> <span class="small"> · Digite seu nome de usuário da rede social (ex.: jonnysmith)!</span></label>
                         <div class="input-group mb-3">
                             <span class="input-group-text">https://www.instagram.com/</span>
                             <input type="text" name="instagram" class="form-control<?= $validation->getError('instagram') ? ' is-invalid' : '' ?>" value="<?= set_value('instagram', $user[0]->instagram) ?>">
                         </div>
                         <div class="input-group mb-3">
                             <span class="input-group-text">https://www.facebook.com/</span>
                             <input type="text" name="facebook" class="form-control<?= $validation->getError('facebook') ? ' is-invalid' : '' ?>" value="<?= set_value('facebook', $user[0]->facebook) ?>">
                         </div>
                         <div class="input-group mb-3">
                             <span class="input-group-text">https://www.twitter.com/</span>
                             <input type="text" name="twitter" class="form-control<?= $validation->getError('twitter') ? ' is-invalid' : '' ?>" value="<?= set_value('twitter', $user[0]->twitter) ?>">
                         </div>
                         <div class="form-group col-md-12">
                             <span class='small text-danger'>
                                 <?php
                                    if ($validation->getError('instagram') || $validation->getError('facebook') || $validation->getError('twitter')) {
                                        echo 'Os nomes de usuários das redes sociais devem ter no máximo 40 caracteres!';
                                    }
                                    ?>
                             </span>
                         </div>
                         <hr>
                         <div class="form-row">
                             <div type="submit" class="form-group col-md-6">
                                 <button class="btn btn-dark col-12"><i class="mdi mdi-cached"></i> SALVAR</button>
                             </div>
                             <div type="button" class="form-group col-md-6">
                                 <a href="<?= base_url('admin/perfil') ?>" class="btn btn-secondary col-12">
                                     <i class="mdi mdi-close"></i> CANCELAR</a>
                             </div>
                         </div>
                     </form>
                 </div>
                 <div class="card-footer text-danger small"><em>* campos obrigatórios!</em></div>
             </div>
         </div>
     </div>
 </div>