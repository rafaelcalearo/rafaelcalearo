 <div class="container-fluid">
     <nav aria-label="breadcrumb">
         <ol class="breadcrumb small">
             <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">casa</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/dashboard') ?>">dashboard</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/perfil') ?>">perfil</a></li>
             <li class="breadcrumb-item active" aria-current="page">editar endereço</li>
         </ol>
     </nav>
     <div class="d-sm-flex align-items-center justify-content-between mb-4">
         <h1 class="h4 mb-0 text-gray-800"><strong>ENDEREÇO</strong></h1>
     </div>
     <div class="row">
         <div class="col-xl-4 col-lg-6">
             <div class="card shadow h-100">
                 <div class="card-header mt-4 text-center">
                     <img src="<?= base_url($user[0]->photo) ?>" class="rounded-circle mb-2" alt="imagem do perfil" width="150" height="150">
                     <h5 class="text-gray-900 mb-0"><?= $user[0]->name ?> <?= $user[0]->surname ?></h5>
                     <p class="text-gray-500 mt-0">
                         <em><?= $user[0]->profile === "ADMIN" ? 'Administrador' : 'Usuário' ?> do sistema</em>
                     </p>
                 </div>
                 <div class="card-body">
                     <ul class="list-group list-group-flush">
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil') ?>">Perfil</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-perfil') ?>">Dados básicos</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-foto') ?>">Foto</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-endereco') ?>"><strong>Endereço</strong></a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-conta') ?>">Conta</a>
                         </li>
                         <li class="list-group-item"><a href="">Encerrar conta</a></li>
                     </ul>
                 </div>
             </div>
         </div>
         <div class="col-xl-8 col-lg-6">
             <div class="card shadow h-100">
                 <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary">ALTERAR ENDEREÇO</h6>
                     <?php $validation = \Config\Services::validation(); ?>
                 </div>
                 <div class="card-body">
                     <form method="post" action="<?= base_url('admin/perfil/editar-endereco') ?>" enctype="multipart/form-data">
                         <?= csrf_field() ?>
                         <div class="form-group">
                             <label for="zip_code"><strong>CEP</strong> <span class="text-danger">*</span></label>
                             <input type="text" name="zip_code" class="form-control<?= $validation->getError('zip_code') ? ' is-invalid' : '' ?>" value="<?= set_value('zip_code', $user[0]->zip_code === '—' ? '' : $user[0]->zip_code) ?>" id="zip_code" />
                             <span class='small text-danger'><?= $validation->getError('zip_code') ?></span>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-8">
                                 <label for="street"><strong>Rua</strong> <span class="text-danger">*</span></label>
                                 <input type="text" name="street" class="form-control<?= $validation->getError('street') ? ' is-invalid' : '' ?>" value="<?= set_value('street', $user[0]->street === '—' ? '' : $user[0]->street) ?>" id="street" />
                                 <span class='small text-danger'><?= $validation->getError('street') ?></span>
                             </div>
                             <div class="form-group col-md-4">
                                 <label for="number"><strong>Número</strong> <span class="text-danger">*</span></label>
                                 <input type="number" name="number" class="form-control<?= $validation->getError('number') ? ' is-invalid' : '' ?>" value="<?= set_value('number', $user[0]->number === '—' ? '' : $user[0]->number) ?>" id="number" />
                                 <span class='small text-danger'><?= $validation->getError('number') ?></span>
                             </div>
                         </div>
                         <div class="form-group">
                             <label for="complement"><strong>Complemento</strong></label>
                             <input type="text" name="complement" class="form-control" value="<?= set_value('complement', $user[0]->complement === '—' ? null : $user[0]->complement) ?>" id="complement" />
                         </div>
                         <div class="form-group">
                             <label for="district"><strong>Bairro</strong> <span class="text-danger">*</span></label>
                             <input type="text" name="district" class="form-control<?= $validation->getError('district') ? ' is-invalid' : '' ?>" value="<?= set_value('district', $user[0]->district === '—' ? '' : $user[0]->district) ?>" id="district" />
                             <span class='small text-danger'><?= $validation->getError('district') ?></span>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-8">
                                 <label for="cidade"><strong>Cidade</strong> <span class="text-danger">*</span></label>
                                 <input type="text" name="city" class="form-control<?= $validation->getError('city') ? ' is-invalid' : '' ?>" value="<?= set_value('city', $user[0]->city === '—' ? '' : $user[0]->city) ?>" id="city" />
                                 <span class='small text-danger'><?= $validation->getError('city') ?></span>
                             </div>
                             <div class="form-group col-md-4">
                                 <label for="state"><strong>UF</strong> <span class="text-danger">*</span></label>
                                 <input type="text" name="state" class="form-control<?= $validation->getError('state') ? ' is-invalid' : '' ?>" value="<?= set_value('state', $user[0]->state === '—' ? '' : $user[0]->state) ?>" id="state" />
                                 <span class='small text-danger'><?= $validation->getError('state') ?></span>
                             </div>
                         </div>
                         <hr>
                         <div class="form-row">
                             <div type="submit" class="form-group col-md-6"><button class="btn btn-dark col-12">
                                     <i class="mdi mdi-cached"></i> SALVAR</button></div>
                             <div type="button" class="form-group col-md-6"> <a href="<?= base_url('admin/perfil') ?>" class="btn btn-secondary col-12">
                                     <i class="mdi mdi-close"></i> CANCELAR</a></div>
                         </div>
                     </form>
                 </div>
                 <div class="card-footer text-danger small"><em>* campos obrigatórios!</em></div>
             </div>
         </div>
     </div>
 </div>