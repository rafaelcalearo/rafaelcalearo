 <!-- Begin Page Content -->
 <div class="container-fluid">

     <nav aria-label="breadcrumb">
         <ol class="breadcrumb small">
             <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">casa</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/dashboard') ?>">dashboard</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/usuarios') ?>">usuários</a></li>
             <li class="breadcrumb-item active" aria-current="page">cadastro</li>
         </ol>
     </nav>

     <!-- Page Heading -->
     <div class="d-sm-flex align-items-center justify-content-between mb-4">
         <h1 class="h4 mb-0 text-gray-800"><strong>CADASTRO</strong></h1>
         <a href="<?= base_url('admin/usuarios') ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">VOLTAR</a>
     </div>

     <!-- Area Chart -->
     <div class="card shadow mb-4">
         <div class="card-header py-3">
             <h6 class="m-0 font-weight-bold text-primary">NOVO USUÁRIO</h6>
             <?php $validation = \Config\Services::validation(); ?>
         </div>
         <div class="card-body">
             <form method="post" action="<?= base_url('admin/usuarios/cadastro') ?>">
                 <?= csrf_field() ?>
                 <div class="form-row">
                     <div class="form-group col-md-6">
                         <label for="name"><strong>Nome</strong>
                             <span class="text-danger">*</span>
                         </label>
                         <input id="name" type="text" class="<?= $validation->getError('name') ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm' ?>" name="name" />
                         <span class='small text-danger'>
                             <?= $validation->getError('name') ? $validation->getError('name') : '' ?>
                         </span>
                     </div>
                     <div class="form-group col-md-6">
                         <label for="surname"><strong>Sobrenome</strong>
                             <span class="text-danger">*</span>
                         </label>
                         <input id="surname" type="text" class="<?= $validation->getError('surname') ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm' ?>" name="surname" />
                         <span class='small text-danger'>
                             <?= $validation->getError('surname') ? $validation->getError('surname') : '' ?>
                         </span>
                     </div>
                 </div>
                 <div class="form-group">
                     <label for="email"><strong>E-mail</strong>
                         <span class="text-danger">*</span>
                     </label>
                     <input id="email" type="email" class="<?= $validation->getError('email') ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm' ?>" name="email" />
                     <span class='small text-danger'>
                         <?= $validation->getError('email') ? $validation->getError('email') : '' ?>
                     </span>
                 </div>
                 <hr>
                 <div class="form-row">
                     <div class="form-group col-md-4">
                         <label for="cep"><strong>CEP</strong>
                             <span class="text-danger">*</span></label>
                         <input id="cep" type="text" class="form-control form-control-sm" />
                     </div>
                     <div class="form-group col-md-8">
                         <label for="rua"><strong>Rua</strong>
                             <span class="text-danger">*</span></label>
                         <input id="rua" type="text" class="form-control form-control-sm" />
                     </div>
                     <div class="form-group col-md-9">
                         <label for="cidade"><strong>Cidade</strong>
                             <span class="text-danger">*</span></label>
                         <input id="cidade" type="text" class="form-control form-control-sm" />
                     </div>
                     <div class="form-group col-md-3">
                         <label for="uf"><strong>UF</strong>
                             <span class="text-danger">*</span></label>
                         <input id="uf" type="text" class="form-control form-control-sm" />
                     </div>
                 </div>
                 <div class="form-row">
                     <div class="form-group col-md-4">
                         <label for="numero"><strong>Número</strong>
                             <span class="text-danger">*</span></label>
                         <input id="numero" type="number" class="form-control form-control-sm" />
                     </div>
                     <div class="form-group col-md-4">
                         <label for="complemento"><strong>Complemento</strong></label>
                         <input id="complemento" type="text" class="form-control form-control-sm" />
                     </div>
                     <div class="form-group col-md-4">
                         <label for="bairro"><strong>Bairro</strong>
                             <span class="text-danger">*</span></label>
                         <input id="bairro" type="text" class="form-control form-control-sm" />
                     </div>
                 </div>
                 <hr>
                 <div class="form-row">
                     <div class="form-group col-md-4">
                         <label for="celular"><strong>Celular</strong></label>
                         <input id="celular" type="text" class="form-control form-control-sm" />
                     </div>
                     <div class="form-group col-md-4">
                         <label for="whatsapp"><strong>WhatsApp</strong></label>
                         <input id="whatsapp" type="text" class="form-control form-control-sm" />
                     </div>
                     <div class="form-group col-md-4">
                         <label for="residencial"><strong>Telefone residencial</strong></label>
                         <input id="residencial" type="text" class="form-control form-control-sm" />
                     </div>
                 </div>
                 <hr>
                 <div class="form-group">
                     <label for="foto"><strong>Foto (Perfil)</strong>
                         <span class="aviso-senha small">· ideal 150 x 150 pixels</span>
                     </label>
                     <input id="foto" type="file" class="form-control-file form-control-sm" />
                 </div>
                 <hr>
                 <div class="form-row">
                     <div class="form-group col-md-4">
                         <label for="facebook"><strong>Facebook (URL)</strong></label>
                         <input id="facebook" type="text" class="form-control form-control-sm" />
                     </div>

                     <div class="form-group col-md-4">
                         <label for="twitter"><strong>Twitter (URL)</strong></label>
                         <input id="twitter" type="text" class="form-control form-control-sm" />
                     </div>
                     <div class="form-group col-md-4">
                         <label for="instagram"><strong>Instagram (URL)</strong></label>
                         <input id="instagram" type="text" class="form-control form-control-sm" />
                     </div>
                 </div>
                 <hr>
                 <div class="form-row">
                     <div class="form-group col-md-4"><button class="btn btn-success btn-sm col-12" type="submit">
                             <i class="mdi mdi-cached"></i> CADASTRAR</button></div>
                     <div class="form-group col-md-4"> <button class="btn btn-primary btn-sm col-12" type="reset">
                             <i class="mdi mdi-broom"></i> LIMPAR</button></div>
                     <div class="form-group col-md-4"> <a href="/admin/usuarios" class="btn btn-secondary btn-sm col-12" type="reset">
                             <i class="mdi mdi-close"></i> CANCELAR</a></div>
                 </div>
             </form>
         </div>
         <div class="card-footer text-danger"><em>* campos obrigatórios!</em></div>
     </div>
 </div>
 <!-- /.container-fluid -->