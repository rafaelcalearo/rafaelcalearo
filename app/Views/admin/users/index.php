 <!-- Begin Page Content -->
 <div class="container-fluid">

     <nav aria-label="breadcrumb">
         <ol class="breadcrumb small">
             <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">casa</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/dashboard') ?>">dashboard</a></li>
             <li class="breadcrumb-item active" aria-current="page">usuários</li>
         </ol>
     </nav>

     <!-- Page Heading -->
     <div class="d-sm-flex align-items-center justify-content-between mb-4">
         <h1 class="h4 mb-0 text-gray-800"><strong>USUÁRIOS</strong></h1>
         <a href="<?= base_url('admin/usuarios/cadastro') ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">NOVO USUÁRIO</a>
     </div>

     <!-- DataTales Example -->
     <div class="card shadow mb-4">
         <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
             <h6 class="m-0 font-weight-bold text-primary">GERENCIAR USUÁRIOS</h6>
             <div class="dropdown no-arrow">
                 <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-600"></i>
                 </a>
                 <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                     <div class="dropdown-header">Dropdown Header:</div>
                     <a class="dropdown-item" href="#">Action</a>
                     <a class="dropdown-item" href="#">Another action</a>
                     <div class="dropdown-divider"></div>
                     <a class="dropdown-item" href="#">Something else here</a>
                 </div>
             </div>
         </div>
         <div class="card-body">
             <div class="table-responsive">
                 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                         <tr>
                             <th>USUÁRIO</th>
                             <th>E-MAIL</th>
                             <th class="text-center">PERFIL</th>
                             <th class="text-center">CRIADO</th>
                             <th class="nosort text-center">AÇÕES</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php foreach ($users as $user) : ?>
                             <tr>
                                 <td>
                                     <img class="img-profile rounded-circle" src="<?= base_url(esc($user->photo)) ?>" width="30">
                                     <?= esc($user->name) ?> <?= esc($user->surname) ?>
                                 </td>
                                 <td>
                                     <?= esc($user->email) ?></td>
                                 <td class="text-center">
                                     <strong><?= esc($user->profile) ?></strong>
                                 </td>
                                 <td class="text-center">
                                     <?= esc(date('d/m/Y', strtotime($user->created_at))) ?>
                                 </td>
                                 <td class="text-center">
                                     <div class="dropdown no-arrow">
                                         <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                             <i class="fas fa-ellipsis-h fa-sm fa-fw text-gray-600"></i>
                                         </a>
                                         <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                             <div class="dropdown-header">Dropdown Header:</div>
                                             <a class="dropdown-item" href="#">Action</a>
                                             <a class="dropdown-item" href="#">Another action</a>
                                             <div class="dropdown-divider"></div>
                                             <a class="dropdown-item" href="#">Something else here</a>
                                         </div>
                                     </div>
                                 </td>
                             </tr>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
             </div>
         </div>
     </div>

 </div>
 <!-- /.container-fluid -->