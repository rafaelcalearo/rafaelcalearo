 <div class="container-fluid">
     <nav aria-label="breadcrumb">
         <ol class="breadcrumb small">
             <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">casa</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/dashboard') ?>">dashboard</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/perfil') ?>">perfil</a></li>
             <li class="breadcrumb-item active" aria-current="page">editar conta</li>
         </ol>
     </nav>
     <div class="d-sm-flex align-items-center justify-content-between mb-4">
         <h1 class="h4 mb-0 text-gray-800"><strong>CONTA</strong></h1>
     </div>
     <div class="row">
         <div class="col-xl-4 col-lg-6">
             <div class="card shadow h-100">
                 <div class="card-header mt-4 text-center">
                     <img src="<?= base_url($user[0]->photo) ?>" class="rounded-circle mb-2" alt="imagem do perfil" width="150" height="150">
                     <h5 class="text-gray-900 mb-0"><?= $user[0]->name ?> <?= $user[0]->surname ?></h5>
                     <p class="text-gray-500 mt-0">
                         <em><?= $user[0]->profile === "ADMIN" ? 'Administrador' : 'Usuário' ?> do sistema</em>
                     </p>
                 </div>
                 <div class="card-body">
                     <ul class="list-group list-group-flush">
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil') ?>">Perfil</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-perfil') ?>">Dados básicos</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-foto') ?>">Foto</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-endereco') ?>">Endereço</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-conta') ?>"><strong>Conta</strong></a>
                         </li>
                         <li class="list-group-item"><a href="">Encerrar conta</a></li>
                     </ul>
                 </div>
             </div>
         </div>
         <div class="col-xl-8 col-lg-6">
             <div class="card shadow h-100">
                 <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary">ALTERAR DADOS DA CONTA</h6>
                     <?php $validation = \Config\Services::validation(); ?>
                 </div>
                 <div class="card-body">
                     <form method="post" action="<?= base_url('admin/perfil/editar-conta') ?>">
                         <?= csrf_field() ?>
                         <div class="form-group">
                             <label for="email"><strong>E-mail</strong> <span class="text-danger">*</span></label>
                             <input type="email" name="email" class="form-control<?= $validation->getError('email') ? ' is-invalid' : '' ?>" value="<?= set_value('email', $user[0]->email) ?>" id="email" />
                             <span class='small text-danger'><?= $validation->getError('email') ?></span>
                         </div>
                         <hr>
                         <div class="form-group">
                             <label for="current_password"><strong>Senha atual</strong> <span class="text-danger">*</span></label>
                             <input type="password" name="current_password" class="form-control<?= $errorCurrentPassword ? ' is-invalid' : '' ?>" placeholder="Digite a senha atual" id="current_password" />
                             <span class='small text-danger'><?= $errorCurrentPassword ?></span>
                         </div>
                         <div class="form-group">
                             <label for="password"><strong>Nova senha</strong> <span class="text-danger">*</span> <span class="small"> · Senha deve conter no mínimo 8 digitos!</span></label>
                             <input type="password" name="password" class="form-control<?= $validation->getError('password') ? ' is-invalid' : '' ?>" placeholder="Digite a nova senha" id="password" />
                             <span class='small text-danger'><?= $validation->getError('password') ?></span>
                         </div>
                         <div class="form-group">
                             <label for="confirm"><strong>Confirmar nova senha</strong> <span class="text-danger">*</span></label>
                             <input type="password" name="confirm" class="form-control<?= $validation->getError('confirm') ? ' is-invalid' : '' ?>" placeholder="Confirme a nova senha" id="confirm" />
                             <span class='small text-danger'><?= $validation->getError('confirm') ?></span>
                         </div>
                         <hr>
                         <div class="form-row">
                             <div type="submit" class="form-group col-md-6">
                                 <button class="btn btn-dark col-12"><i class="mdi mdi-cached"></i> SALVAR</button>
                             </div>
                             <div type="button" class="form-group col-md-6">
                                 <a href="<?= base_url('admin/perfil') ?>" class="btn btn-secondary col-12">
                                     <i class="mdi mdi-close"></i> CANCELAR</a>
                             </div>
                         </div>
                     </form>
                 </div>
                 <div class="card-footer text-danger small"><em>* campos obrigatórios!</em></div>
             </div>
         </div>
     </div>
 </div>