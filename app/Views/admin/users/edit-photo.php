 <div class="container-fluid">
     <nav aria-label="breadcrumb">
         <ol class="breadcrumb small">
             <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">casa</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/dashboard') ?>">dashboard</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/perfil') ?>">perfil</a></li>
             <li class="breadcrumb-item active" aria-current="page">editar foto</li>
         </ol>
     </nav>
     <div class="d-sm-flex align-items-center justify-content-between mb-4">
         <h1 class="h4 mb-0 text-gray-800"><strong>FOTO</strong></h1>
     </div>
     <div class="row">
         <div class="col-xl-4 col-lg-6">
             <div class="card shadow h-100">
                 <div class="card-header mt-4 text-center">
                     <img src="<?= base_url($user[0]->photo) ?>" class="rounded-circle mb-2" alt="imagem do perfil" width="150" height="150">
                     <h5 class="text-gray-900 mb-0"><?= $user[0]->name ?> <?= $user[0]->surname ?></h5>
                     <p class="text-gray-500 mt-0">
                         <em><?= $user[0]->profile === "ADMIN" ? 'Administrador' : 'Usuário' ?> do sistema</em>
                     </p>
                 </div>
                 <div class="card-body">
                     <ul class="list-group list-group-flush">
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil') ?>">Perfil</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-perfil') ?>">Dados básicos</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-foto') ?>"><strong>Foto</strong></a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-endereco') ?>">Endereço</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-conta') ?>">Conta</a>
                         </li>
                         <li class="list-group-item"><a href="">Encerrar conta</a></li>
                     </ul>
                 </div>
             </div>
         </div>
         <div class="col-xl-8 col-lg-6">
             <div class="card shadow h-100">
                 <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary">ALTERAR FOTO</h6>
                     <?php $validation = \Config\Services::validation(); ?>
                 </div>
                 <div class="card-body">
                     <form method="post" action="<?= base_url('admin/perfil/editar-foto') ?>" enctype="multipart/form-data">
                         <?= csrf_field() ?>
                         <div class="form-group">
                             <label for="name"><strong>Visualização da imagem</strong></label>
                             <div class="mt-3 mb-4 text-center">
                                 <img src="<?= base_url($user[0]->photo) ?>" class="img-thumbnail" id="preview-img" alt="<?= $user[0]->name . ' ' .  $user[0]->surname ?>" width="150" height="150">
                             </div>
                         </div>
                         <hr>
                         <div class="mb-3">
                             <label for="formFile" class="form-label">
                                 <strong>Imagem</strong> <span class="text-danger">*</span> <span class="small"> · Ideal 150 x 150 pixels (quadrada) e o tamanho máximo de arquivo 1024MB!</span>
                             </label>
                             <input type="file" name="file" class="form-control<?= $validation->getError('file') ? ' is-invalid' : '' ?>" onchange="previewImage()">
                             <span class='small text-danger mt-1'><?= $validation->getError('file') ?></span>
                         </div>
                         <hr>
                         <div class="form-row">
                             <div type="submit" class="form-group col-md-6"><button class="btn btn-dark col-12">
                                     <i class="mdi mdi-cached"></i> SALVAR</button></div>
                             <div type="button" class="form-group col-md-6"> <a href="<?= base_url('admin/perfil') ?>" class="btn btn-secondary col-12">
                                     <i class="mdi mdi-close"></i> CANCELAR</a></div>
                         </div>
                     </form>
                 </div>
                 <div class="card-footer text-danger small"><em>* campo obrigatório!</em></div>
             </div>
         </div>
     </div>
 </div>