 <div class="container-fluid">
     <nav aria-label="breadcrumb">
         <ol class="breadcrumb small">
             <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">casa</a></li>
             <li class="breadcrumb-item"><a href="<?= base_url('admin/dashboard') ?>">dashboard</a></li>
             <li class="breadcrumb-item active" aria-current="page">perfil</li>
         </ol>
     </nav>
     <div class="d-sm-flex align-items-center justify-content-between mb-4">
         <h1 class="h4 mb-0 text-gray-800"><strong>PERFIL</strong></h1>
     </div>
     <div class="row">
         <div class="col-xl-4 col-lg-4">
             <div class="card shadow h-100">
                 <div class="card-header mt-4 text-center">
                     <img src="<?= base_url($user[0]->photo) ?>" class="rounded-circle mb-2" alt="imagem do perfil" width="150" height="150">
                     <h5 class="text-gray-900 mb-0"><?= $user[0]->name ?> <?= $user[0]->surname ?></h5>
                     <p class="text-gray-500 mt-0">
                         <em><?= $user[0]->profile === "ADMIN" ? 'Administrador' : 'Usuário' ?> do sistema</em>
                     </p>
                 </div>
                 <div class="card-body">
                     <ul class="list-group list-group-flush">
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil') ?>"><strong>Perfil</strong></a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-perfil') ?>">Dados básicos</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-foto') ?>">Foto</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-endereco') ?>">Endereço</a>
                         </li>
                         <li class="list-group-item">
                             <a href="<?= base_url('admin/perfil/editar-conta') ?>">Conta</a>
                         </li>
                         <li class="list-group-item"><a href="">Encerrar conta</a></li>
                     </ul>
                 </div>
             </div>
         </div>
         <div class="col-xl-4 col-lg-4">
             <div class="card shadow h-100">
                 <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary">PERFIL</h6>
                 </div>
                 <div class="card-body">
                     <small><span class="small">ENDEREÇO DE E-MAIL</span></small>
                     <h6 class="text-gray-900"><strong><?= empty($user[0]->email) ? '—' : $user[0]->email ?></strong></h6>
                     <small><span class="small">CELULAR</span></small>
                     <h6 class="text-gray-900"><strong><?= empty($user[0]->cell) ? '—' : $user[0]->cell ?></strong></h6>
                     <small><span class="small">WHATSAPP</span></small>
                     <h6 class="text-gray-900"><strong><?= empty($user[0]->whatsapp) ? '—' : $user[0]->whatsapp ?></strong></h6>
                     <small><span class="small">TELEFONE RESIDENCIAL</span></small>
                     <h6 class="text-gray-900"><strong><?= empty($user[0]->phone) ? '—' : $user[0]->phone ?></strong></h6>
                     <small><span class="small">ENDEREÇO</span></small>
                     <h6 class="text-gray-900">
                         <strong>
                             <?= empty($user[0]->street) ? '—' : $user[0]->street . ', n. ' . $user[0]->number
                                    . (empty($user[0]->complement) ? '' : ' (' . $user[0]->complement . ')') ?>
                         </strong>
                     </h6>
                     <small><span class="small">BAIRRO</span></small>
                     <h6 class="text-gray-900"><strong><?= empty($user[0]->district) ? '—' : $user[0]->district ?></strong></h6>
                     <small><span class="small">CEP</span></small>
                     <h6 class="text-gray-900"><strong><?= empty($user[0]->zip_code) ? '—' : $user[0]->zip_code ?></strong></h6>
                     <small><span class="small">CIDADE (UF)</span></small>
                     <h6 class="text-gray-900"><strong>
                             <?= empty($user[0]->city) ? '—' : $user[0]->city . ' (' . $user[0]->state . ')' ?>
                         </strong></h6>
                     <small><span class="small">REDES SOCIAIS</span></small>
                     <br />
                     <?php
                        if (empty($user[0]->facebook) && empty($user[0]->instagram) && empty($user[0]->twitter)) {
                            echo '<h6 class="text-gray-900"><strong>—</strong></h6>';
                        }
                        if (!empty($user[0]->facebook)) {
                            echo '<a href="https://www.facebook.com/' . $user[0]->facebook . '" target="_blank" class="btn btn-circle btn-secondary mr-1"><i class="fab fa-facebook-f"></i></a>';
                        }
                        if (!empty($user[0]->instagram)) {
                            echo '<a href="https://www.instagram.com/' . $user[0]->instagram . '" target="_blank" class="btn btn-circle btn-secondary mr-1"><i class="fab fa-instagram"></i></a>';
                        }
                        if (!empty($user[0]->twitter)) {
                            echo '<a href="https://twitter.com/' . $user[0]->twitter . '" target="_blank" class="btn btn-circle btn-secondary"><i class="fab fa-twitter"></i></a>';
                        }
                        ?>
                     <div class="mt-4 text-center">
                         <span class="tiny text-gray-600">
                             <em>ÚLTIMA ATUALIZAÇÃO EM:
                                 <?= strtotime($user[0]->userupdate) > strtotime($user[0]->updated_at) ? date('d/m/Y', strtotime($user[0]->userupdate)) : (strtotime($user[0]->userupdate) == strtotime($user[0]->updated_at) ? date('d/m/Y', strtotime($user[0]->userupdate)) : date('d/m/Y', strtotime($user[0]->updated_at))) ?>
                             </em>
                         </span>
                     </div>
                 </div>
             </div>
         </div>
         <div class="col-xl-4 col-lg-4">
             <div class="card shadow h-100">
                 <div class="card-header py-3">
                     <h6 class="m-0 font-weight-bold text-primary">LOCALIZAÇÃO</h6>
                 </div>
                 <div class="card-body">
                     <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d65193779.752580315!2d-2.3492343561722606!3d4.198242496589812!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1624639885461!5m2!1spt-BR!2sbr" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
                 </div>
             </div>
         </div>
     </div>
 </div>