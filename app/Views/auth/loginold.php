<form action="/login" method="POST" class="user">
	<!--<input type="hidden" name="?= csrf_token() ?>" value="<?= csrf_hash() ?>" />-->
	<div class="form-group">
		<input type="email" value="<?php echo set_value('email'); ?>" class="<?php if ($validation->getError('email')) {
																					echo 'form-control form-control-user is-invalid';
																				} else {
																					echo 'form-control form-control-user';
																				} ?>" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Seu e-mail" name="email">
		<?php if ($validation->getError('email')) { ?>
			<span class='small text-danger ml-3'>
				<?php echo $validation->getError('email'); ?>
			</span>
		<?php } ?>
	</div>
	<div class="form-group">
		<input type="password" value="<?php echo set_value('password'); ?>" class="<?php if ($validation->getError('password')) {
																						echo 'form-control form-control-user is-invalid';
																					} else {
																						echo 'form-control form-control-user';
																					} ?>" id="exampleInputPassword" placeholder="Senha" name="password">
		<?php if ($validation->getError('password')) { ?>
			<span class='small text-danger ml-3'>
				<?php echo $validation->getError('password'); ?>
			</span>
		<?php } ?>
	</div>
	<div class="form-group">
		<div class="custom-control custom-checkbox small">
			<input type="checkbox" class="custom-control-input" id="customCheck">
			<label class="custom-control-label" for="customCheck">Lembrar de mim</label>
		</div>
	</div>
	<button class="btn btn-primary btn-user btn-block">
		FAZER LOGIN
	</button>
	<hr>
	<a href="index.html" class="btn btn-google btn-user btn-block">
		<i class="fab fa-google fa-fw"></i> COM O GOOGLE
	</a>
	<a href="index.html" class="btn btn-facebook btn-user btn-block">
		<i class="fab fa-facebook-f fa-fw"></i> COM O FACEBOOK
	</a>
</form>
<hr>
<div class="text-center">
	<a class="small" href="<?php echo base_url('#') ?>">Esqueceu a senha?</a>
</div>
<div class="text-center">
	Ainda sem conta? <a class="small" href="<?php echo base_url('register') ?>">Cadastre-se!</a>
</div>