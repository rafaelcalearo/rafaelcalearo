<?php

namespace App\Controllers;
use App\Models\UserModel;

class Register extends BaseController
{
	public function index()
	{
		helper(['form']);
		return view('admin/login/register');
	}

	public function new()
	{
		helper(['form']);

		$input = $this->validate([
			'name' => [
				'label' => 'nome', 'rules' => 'required|min_length[2]',
				'errors' => [
					'required' => 'Preencha este campo!',
					'min_length' => 'No mínimo 2 digitos!'
				]
			],
			'surname' => [
				'label' => 'Sobrenome', 'rules' => 'required|min_length[2]',
				'errors' => [
					'required' => 'Preencha este campo!',
					'min_length' => 'No mínimo 2 digitos!'
				]
			],
			'email' => [
				'label' => 'Email', 'rules' => 'required|valid_email',
				'errors' => [
					'required' => 'Preencha este campo!',
					'valid_email' => 'O e-mail deve ser válido!'
				]
			],
			'password' => [
				'label' => 'Senha', 'rules' => 'required|min_length[8]',
				'errors' => [
					'required' => 'Preencha este campo!',
					'min_length' => 'No mínimo 8 digitos!'
				]
			],
			'repeat' => [
				'label' => 'Repetir', 'rules' => 'required|min_length[8]|matches[password]',
				'errors' => [
					'required' => 'Preencha este campo!',
					'min_length' => 'Deve ser igual a senha!',
					'matches' => 'Deve ser igual a senha!'
				]
			],
		]);

		if (!$input) {
			echo view('admin/login/register', ['validation' => $this->validator]);
		} else {
			$data = [
				'name' => trim($this->request->getPost('name')) . ' ' . trim($this->request->getPost('surname')),
				'email' => $this->request->getPost('email'),
				'password' => password_hash($this->request->getPost('password'), PASSWORD_DEFAULT, ['cost' => 8]),
				'perfil' => 'public'
			];
			$UserModel = new UserModel();
			if ($UserModel->save($data)) {
				//sucesso
				$this->session->setFlashdata('sucesso', 'Cadastro realizado com sucesso!');
			} else {
				//erro
				$this->session->setFlashdata('erro', 'Não foi possível realizar o cadastro!');
			}

			return redirect()->to('/login');
			//return $this->response->redirect(site_url('/submit-form'));
		}
	}
}
