<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\UserModel;
use App\Models\UserContactsModel;

class Usuarios extends BaseController
{
	public function index()
	{
		$model = new UserModel();
		echo view('admin/templates/header', ['title' => 'Usuários', 'datatables' => true]);
		echo view('admin/users/index', ['users' => $model->findAll()]);
		echo view('admin/templates/footer', ['datatables' => true]);
	}

	public function profile()
	{
		$session = session();
		$user = new UserModel();
		echo view('admin/templates/header', ['title' => 'Perfil', 'datatables' => false]);
		echo view('admin/users/profile', ['user' => $user->getUser($session->get('id'))]);
		echo view('admin/templates/footer', ['datatables' => false]);
	}

	public function editProfile()
	{
		helper(['form']);
		$session = session();
		$user = new UserModel();
		$contact = new UserContactsModel();
		$validation = true;
		if ($this->request->getMethod() == "post" && $this->validate([
			'name' => ['rules' => 'required|min_length[2]|max_length[30]', 'errors' => ['required' => 'O campo é obrigatório!', 'min_length' => 'Nome muito pequeno!', 'max_length' => 'Nome muito grande!']],
			'surname' => ['rules' => 'required|min_length[2]|max_length[30]', 'errors' => ['required' => 'O campo é obrigatório!', 'min_length' => 'Sobrenome muito pequeno!', 'max_length' => 'Sobrenome muito grande!']]
		])) {
			if ($this->request->getPost('cell')) {
				$validation = $this->validate(['cell' => ['rules' => 'min_length[15]', 'errors' => ['min_length' => 'O campo deve ter 15 digitos!']]]) ? true : false;
			}
			if ($this->request->getPost('whatsapp')) {
				$validation = $this->validate(['whatsapp' => ['rules' => 'min_length[15]', 'errors' => ['min_length' => 'O campo deve ter 15 digitos!']]]) ? true : false;
			}
			if ($this->request->getPost('phone')) {
				$validation = $this->validate(['phone' => ['rules' => 'min_length[14]', 'errors' => ['min_length' => 'O campo deve ter 14 digitos!']]]) ? true : false;
			}
			if (trim($this->request->getPost('instagram'))) {
				$validation = $this->validate(['instagram' => ['rules' => 'max_length[40]']]) ? true : false;
			}
			if (trim($this->request->getPost('facebook'))) {
				$validation = $this->validate(['facebook' => ['rules' => 'max_length[40]']]) ? true : false;
			}
			if (trim($this->request->getPost('twitter'))) {
				$validation = $this->validate(['twitter' => ['rules' => 'max_length[40]']]) ? true : false;
			}
			if ($validation) {
				$user->save([
					'id' => $session->get('id'),
					'name' => trim($this->request->getPost('name')),
					'surname' => trim($this->request->getPost('surname')),
					'gender' => $this->request->getPost('gender')
				]);
				$contact->save([
					'user_id' => $session->get('id'),
					'cell' => $this->request->getPost('cell'),
					'whatsapp' => $this->request->getPost('whatsapp'),
					'phone' => $this->request->getPost('phone'),
					'instagram' => trim($this->request->getPost('instagram')),
					'facebook' => trim($this->request->getPost('facebook')),
					'twitter' => trim($this->request->getPost('twitter'))
				]);
				$session->set(['name' => trim($this->request->getPost('name')) . ' ' . trim($this->request->getPost('surname'))]);
				$session->setFlashdata("success", "Perfil atualizado com sucesso!");
			}
		}
		echo view('admin/templates/header', ['title' => 'Perfil', 'datatables' => false]);
		echo view('admin/users/edit-profile', ['validation' => $this->validator, 'user' => $user->getUser($session->get('id'))]);
		echo view('admin/templates/footer', ['datatables' => false]);
	}

	public function editPhoto()
	{
		helper(['form']);
		$session = session();
		$user = new UserModel();
		if ($this->request->getMethod() == "post" && $this->validate([
			"file" => ["rules" => "uploaded[file]|max_size[file,1024]|is_image[file]|mime_in[file,image/jpg,image/jpeg,image/gif,image/png]",]
		])) {
			$file = $this->request->getFile("file");
			$newName = $file->getRandomName();
			if ($file->move('uploads/users/' . $session->get('id'), $newName)) {
				if ($user->save([
					'id' => $session->get('id'),
					'photo' => 'uploads/users/' . $session->get('id') . '/' . $newName
				])) {
					$session->set(['photo' => 'uploads/users/' . $session->get('id') . '/' . $newName]);
					$session->setFlashdata("success", "Foto atualizada com sucesso!");
				} else {
					$session->setFlashdata("error", "Foto não atualizada!");
				}
			} else {
				$session->setFlashdata("error", "Foto não atualizada!");
			}
		}
		echo view('admin/templates/header', ['title' => 'Perfil', 'datatables' => false]);
		echo view('admin/users/edit-photo', ['validation' => $this->validator, 'user' => $user->getUser($session->get('id'))]);
		echo view('admin/templates/footer', ['datatables' => false]);
	}

	public function editAddress()
	{
		helper(['form']);
		$session = session();
		$user = new UserModel();
		$contact = new UserContactsModel();
		$validation = true;
		if ($this->request->getMethod() == "post" && $this->validate([
			'zip_code' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'street' => ['rules' => 'required|max_length[100]', 'errors' => ['required' => 'O campo é obrigatório!', 'max_length' => 'O campo deve conter no máximo 100 caracteres!']],
			'city' => ['rules' => 'required|max_length[40]', 'errors' => ['required' => 'O campo é obrigatório!', 'max_length' => 'O campo deve conter no máximo 40 caracteres!']],
			'state' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'number' => ['rules' => 'required', 'errors' => ['required' => 'O campo é obrigatório!']],
			'district' => ['rules' => 'required|max_length[40]', 'errors' => ['required' => 'O campo é obrigatório!', 'max_length' => 'O campo deve conter no máximo 40 caracteres!']],
		])) {
			if (trim($this->request->getPost('complement'))) {
				$validation = $this->validate(['complement' => ['rules' => 'max_length[30]', 'errors' => ['required' => 'O campo é obrigatório!', 'max_length' => 'O campo deve conter no máximo 30 caracteres!']]]) ? true : false;
			}
			if ($validation) {
				if ($contact->save([
					'user_id' => $session->get('id'),
					'zip_code' => $this->request->getPost('zip_code'),
					'street' => trim($this->request->getPost('street')),
					'number' => $this->request->getPost('number'),
					'complement' => trim($this->request->getPost('complement')),
					'district' => trim($this->request->getPost('district')),
					'city' => trim($this->request->getPost('city')),
					'state' => $this->request->getPost('state'),
				])) {
					$session->setFlashdata("success", "Endereço atualizado com sucesso!");
				} else {
					$session->setFlashdata("error", "Endereço não atualizado!");
				}
			}
		}
		echo view('admin/templates/header', ['title' => 'Perfil', 'datatables' => false]);
		echo view('admin/users/edit-address', ['validation' => $this->validator, 'user' => $user->getUser($session->get('id'))]);
		echo view('admin/templates/footer', ['datatables' => false]);
	}

	public function editAccount()
	{
		helper(['form']);
		$session = session();
		$user = new UserModel();
		$errorCurrentPassword = null;
		if ($this->request->getMethod() == "post" && $this->validate([
			'email' => ['rules' => 'required|valid_email', 'errors' => ['required' => 'O campo é obrigatório!', 'valid_email' => 'O e-mail não é válido!']],
		])) {
			if ($this->request->getPost('current_password')) {
				$nowUser = $user->find($session->get('id'));
				if (password_verify($this->request->getPost('current_password'), $nowUser->password)) {
					if ($this->validate([
						'password' => ['rules' => 'required|min_length[8]|max_length[15]', 'errors' => ['required' => 'O campo é obrigatório!', 'min_length' => 'Senha deve conter no mínimo 8 digitos!', 'max_length' => 'Senha deve conter no máximo 15 digitos!']],
						'confirm' => ['rules' => 'required|matches[password]', 'errors' => ['required' => 'O campo é obrigatório!', 'matches' => 'Senha diferente da nova senha!']]
					])) {
						if ($user->save([
							'id' => $session->get('id'),
							'email' => trim($this->request->getPost('email')),
							'password' => password_hash($this->request->getPost('password'), PASSWORD_DEFAULT, ['cost' => 8])
						])) {
							$session->set(['email' => trim($this->request->getPost('email'))]);
							$session->setFlashdata("success", "Dados da conta atualizados com sucesso!");
						} else {
							$session->setFlashdata("error", "Dados da conta não atualizados!");
						}
					}
				} else {
					$errorCurrentPassword = "Senha atual não confere!";
				}
			} else {
				if ($user->save(['id' => $session->get('id'), 'email' => $this->request->getPost('email')])) {
					$session->set(['email' => trim($this->request->getPost('email'))]);
					$session->setFlashdata("success", "E-mail da conta atualizado com sucesso!");
				} else {
					$session->setFlashdata("error", "E-mail da conta não atualizado!");
				}
			}
		}
		echo view('admin/templates/header', ['title' => 'Perfil', 'datatables' => false]);
		echo view('admin/users/edit-account', [
			'validation' => $this->validator,
			'user' => $user->getUser($session->get('id')),
			'errorCurrentPassword' => $errorCurrentPassword,
			'datatables' => false
		]);
		echo view('admin/templates/footer');
	}
}
