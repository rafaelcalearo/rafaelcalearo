<?php

namespace App\Controllers;

use App\Models\UserModel;

class Login extends BaseController
{
	public function index()
	{
		helper('form');
		$session = session();
		if ($this->request->getMethod() === 'post' && $this->validate([
			'email' => ['rules' => 'required|valid_email', 'errors' => ['required' => 'Preencha este campo!', 'valid_email' => 'E-mail inválido!']],
			'password' => ['rules' => 'required|min_length[8]', 'errors' => ['required' => 'Preencha este campo!', 'min_length' => 'No mínimo 8 digitos!']]
		])) {
			$model = new UserModel();
			$user = $model->getUserEmail($this->request->getPost('email'));
			if (isset($user) && password_verify($this->request->getPost('password'), $user->password)) {
				$session->set([
					'id' => $user->id,
					'name' => $user->name . ' ' . $user->surname,
					'email' => $user->email,
					'photo' => $user->photo,
					'logged_in' => true
				]);
				return redirect()->to(base_url('admin/dashboard'));
			} else {
				echo view('auth/login', ['error' => 'Email/senha incorreta ou não cadastrada!']);
			}
		} else {
			echo view('auth/login', ['error' => null]);
		}
	}

	public function logout()
	{
		$session = session();
		$session->destroy();
		return redirect()->to(base_url('login'));
	}
}
