<?php

namespace App\Models;

use CodeIgniter\Model;

class UserContactsModel extends Model
{
    protected $table = 'usercontacts';
    protected $primaryKey = 'user_id';

    protected $returnType = 'object';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['zip_code', 'street', 'number', 'complement', 'district', 'city', 'state', 'cell', 'whatsapp', 'phone', 'facebook', 'twitter', 'instagram'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
}
