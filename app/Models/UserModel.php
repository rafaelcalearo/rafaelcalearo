<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType = 'object';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['name', 'surname', 'email', 'password', 'profile', 'photo', 'gender'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    public function getUserEmail($email)
    {
        return $this->where(['email' => $email])->first();
    }

    public function getUser($id = NULL)
    {
        return $this->select('users.name, users.surname, users.email, users.profile, users.photo, users.gender, users.updated_at as userupdate, '
            . 'usercontacts.zip_code, usercontacts.street, usercontacts.number, usercontacts.complement, '
            . 'usercontacts.district, usercontacts.city, usercontacts.state, usercontacts.cell, '
            . 'usercontacts.whatsapp, usercontacts.phone, usercontacts.facebook, usercontacts.twitter, usercontacts.instagram, usercontacts.updated_at')
            ->join('usercontacts', 'usercontacts.user_id = users.id')
            ->where('users.id', $id)
            ->find();
    }
}
